package unit2.lab1;

import java.io.File;
import java.io.IOException;

import unbbayes.io.BaseIO;
import unbbayes.io.NetIO;
import unbbayes.prs.Edge;
import unbbayes.prs.bn.JunctionTreeAlgorithm;
import unbbayes.prs.bn.PotentialTable;
import unbbayes.prs.bn.ProbabilisticNetwork;
import unbbayes.prs.bn.ProbabilisticNode;
import unbbayes.prs.exception.InvalidParentException;
import unbbayes.util.extension.bn.inference.IInferenceAlgorithm;

public class exe4_ExtendBasicExample {
	public static final String FOLDER = "src/main/resources/";

	public static void main(String[] args) {
		// Load BN
		ProbabilisticNetwork net = null;

		try {
			BaseIO io = new NetIO(); // open a .net file
			net = (ProbabilisticNetwork) io.load(new File(FOLDER + "exe2_Sprinkler.net"));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

		// add new variable
		// adding a new node manually
		ProbabilisticNode fallNode = new ProbabilisticNode();
		fallNode.setName("Fall");
		fallNode.setDescription("Define if a person fall or not");
		fallNode.appendState("True");
		fallNode.appendState("False");
		net.addNode(fallNode);

		// create the probabilistic table
		PotentialTable auxCPT = fallNode.getProbabilityFunction();
		auxCPT.addVariable(fallNode);
		auxCPT.setValue(0, 0.2f);
		auxCPT.setValue(1, 0.8f);
		
		// adding a new edge manually
		ProbabilisticNode pavementNode = (ProbabilisticNode) net.getNode("Pavement");
		Edge pavToFall = new Edge(pavementNode, fallNode);
		try {
			net.addEdge(pavToFall);
		} catch (InvalidParentException e) {
			e.printStackTrace();
		}

		//adjust cpf based to the edge
		auxCPT.setValue(2, 0.02f);
		auxCPT.setValue(3, 0.98f);

		// compile the network and check errors
		IInferenceAlgorithm algorithm = new JunctionTreeAlgorithm();
		algorithm.setNetwork(net);
		algorithm.run();

		// save in .net
		File file_net = new File(FOLDER + "exe4_Sprinkler_Extended_Fall.net");
		try {
			BaseIO netFormat = new NetIO();
			netFormat.save(file_net, net);
		} catch (IOException e) {
			System.out.println(e.toString());
		}

	}

}
