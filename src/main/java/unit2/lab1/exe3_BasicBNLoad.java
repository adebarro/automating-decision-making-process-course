package unit2.lab1;

import java.io.File;
import java.util.List;

import unbbayes.io.BaseIO;
import unbbayes.io.NetIO;
import unbbayes.prs.Node;
import unbbayes.prs.bn.JunctionTreeAlgorithm;
import unbbayes.prs.bn.PotentialTable;
import unbbayes.prs.bn.ProbabilisticNetwork;
import unbbayes.prs.bn.ProbabilisticNode;
import unbbayes.util.extension.bn.inference.IInferenceAlgorithm;

public class exe3_BasicBNLoad {

	public static final String FOLDER = "src/main/resources/";

	public static void main(String args[]) {

		// Load BN
		ProbabilisticNetwork net = null;

		try {
			BaseIO io = new NetIO(); // open a .net file
			net = (ProbabilisticNetwork) io.load(new File(FOLDER+"exe1_feeding_gastro.net"));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

		// retrieve some nodes and check the values
		ProbabilisticNode tubeSiteNode = (ProbabilisticNode) net.getNode("TubeSite");
		System.out.println("Tube Site Description: " + (tubeSiteNode.getDescription()));

		// get states names
		System.out.println("Tube Site States:");
		for (String stateName : tubeSiteNode.getStatesCopy())
			System.out.println("-" + stateName);

		// get nodes and probabilistic tables
		System.out.println("\n -------- Network Information  -------- \n");
		for (Node node : net.getNodes()) {
			String nodeName = node.getName();
			System.out.println(nodeName);
			ProbabilisticNode pNode = (ProbabilisticNode) node;
			PotentialTable cpt = pNode.getProbabilityFunction();
			for (int i = 0; i < node.getStatesSize(); i++) {
				String stateName = node.getStateAt(i);
				float value = cpt.getValue(i);
				System.out.println("-" + stateName + ":" + value);
			}
			System.out.println("--------");
		}

		// compile the network and check errors
		IInferenceAlgorithm algorithm = new JunctionTreeAlgorithm();
		algorithm.setNetwork(net);
		algorithm.run();

		// print node's prior marginal probabilities
		System.out.println("\n -------- Compile Network  -------- \n");
		List<Node> nodeList = net.getNodes();
		for (Node node : nodeList) {
			System.out.println(node.getName());
			for (int i = 0; i < node.getStatesSize(); i++) {
				System.out.println("-" + node.getStateAt(i) + " : " + ((ProbabilisticNode) node).getMarginalAt(i));
			}
			System.out.println("--------");
		}

		// insert evidence (finding)
		System.out.println("\n -------- set an evidence -> Medication is presented  -------- \n");
		ProbabilisticNode medicationNode = (ProbabilisticNode) net.getNode("Medication");
		for (int i = 0; i < medicationNode.getStatesSize(); i++) {
			String stateName = medicationNode.getStateAt(i);
			if (stateName.equals("Absent")) {
				medicationNode.addFinding(i);
			}
		}

		// propagate evidence
		System.out.println("\n -------- propagate the evidence....  -------- \n");
		try {
			algorithm.propagate();
		} catch (Exception exc) {
			System.out.println(exc.getMessage());
		}
		
        //print updated (posterior) node's marginal probabilities
		System.out.println("\n -------- print updated (posterior) node's marginal probabilities  -------- \n");
		for (Node node : nodeList) {
			System.out.println(node.getName());
			for (int i = 0; i < node.getStatesSize(); i++) {
				System.out.println(node.getStateAt(i) + " : " + ((ProbabilisticNode)node).getMarginalAt(i));
			}
			System.out.println("--------");
		}

	}

}
