package unit5.lab4;

import java.io.File;

import unbbayes.io.BaseIO;
import unbbayes.io.NetIO;
import unbbayes.prs.Edge;
import unbbayes.prs.Node;
import unbbayes.prs.bn.JunctionTreeAlgorithm;
import unbbayes.prs.bn.ProbabilisticNetwork;
import unbbayes.prs.bn.ProbabilisticNode;
import unbbayes.prs.bn.TreeVariable;
import unbbayes.prs.exception.InvalidParentException;
import unbbayes.prs.id.DecisionNode;
import unbbayes.prs.id.UtilityNode;

public class InfluenceDiagramExample {
	
	public static final String FOLDER = "src/main/resources/";

	public static void main(String[] args) throws InvalidParentException {
		// Load BN
		ProbabilisticNetwork net = null;

		try {
			BaseIO io = new NetIO(); // open a .net file
			net = (ProbabilisticNetwork) io.load(new File(FOLDER + "lab3_exe3_industrial_attackgraph.net"));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		DecisionNode dPubInform = new DecisionNode();
		dPubInform.setName("InformExternalPublic");
		dPubInform.setDescription("Inform (or not) external public");
		dPubInform.appendState("inform");
		dPubInform.appendState("no-inform");
		net.addNode(dPubInform);
		
		DecisionNode dActionScope = new DecisionNode();
		dActionScope.setName("Scope of Action");
		dActionScope.setDescription("Inform (or not) external public");
		dActionScope.appendState("soc-local");
		dActionScope.appendState("soc-global");
		net.addNode(dActionScope);
		
		
		net.addEdge(new Edge(dActionScope, dPubInform));
		
		UtilityNode uCompanyValue = new UtilityNode();
		uCompanyValue.setName("CompanyValue");
		uCompanyValue.setDescription("Company Value");
		uCompanyValue.getProbabilityFunction().addVariable(uCompanyValue);
		net.addNode(uCompanyValue);
		
		ProbabilisticNode sabNode = (ProbabilisticNode) net.getNode("SabotagingSystem");
		ProbabilisticNode runsAttackNode = (ProbabilisticNode) net.getNode("RunsAttack");
		
		net.addEdge(new Edge(runsAttackNode, uCompanyValue));
		net.addEdge(new Edge(sabNode, uCompanyValue));
		net.addEdge(new Edge(dPubInform, uCompanyValue));
		
		uCompanyValue.getProbabilityFunction().setValue(0, 400);
		uCompanyValue.getProbabilityFunction().setValue(1, 5000);
		uCompanyValue.getProbabilityFunction().setValue(2, -1000);
		uCompanyValue.getProbabilityFunction().setValue(3, -20000);
		uCompanyValue.getProbabilityFunction().setValue(4, -2000);
		uCompanyValue.getProbabilityFunction().setValue(5, -100);
		uCompanyValue.getProbabilityFunction().setValue(1, -50000);
		

		UtilityNode uRemedCost = new UtilityNode();
		uRemedCost.setName("RemediationCost");
		uRemedCost.setDescription("Remediation Cost");
		uRemedCost.getProbabilityFunction().addVariable(uRemedCost);
		net.addNode(uRemedCost);
		
		ProbabilisticNode c2Node = (ProbabilisticNode) net.getNode("CommandControlServer");
		net.addEdge(new Edge(dActionScope, uRemedCost));
		net.addEdge(new Edge(c2Node, uRemedCost));
		net.addEdge(new Edge(runsAttackNode, uRemedCost));
		
		uRemedCost.getProbabilityFunction().setValue(0, 400);
		uRemedCost.getProbabilityFunction().setValue(1, 5000);
		uRemedCost.getProbabilityFunction().setValue(2, -1000);
		uRemedCost.getProbabilityFunction().setValue(3, -20000);
		uRemedCost.getProbabilityFunction().setValue(4, -2000);
		uRemedCost.getProbabilityFunction().setValue(5, 5000);
		uRemedCost.getProbabilityFunction().setValue(6, -100);
		uRemedCost.getProbabilityFunction().setValue(7, -50000);
		
		JunctionTreeAlgorithm alg = new JunctionTreeAlgorithm();
		alg.setNetwork(net);
		alg.run();
		
		for (Node node : net.getNodes()) {
			System.out.println(node.getDescription());
			for (int i = 0; i < node.getStatesSize(); i++) {
				if (node instanceof TreeVariable) {
					System.out.println(node.getStateAt(i) + " : " 
							+ ((TreeVariable)node).getMarginalAt(i));
				}
			}
		}
		
		
		ProbabilisticNode antiMalNode = (ProbabilisticNode )net.getNode("AntiMalwareVulnerability");
		for (int i = 0; i < antiMalNode.getStatesSize(); i++) {
			String stateName = antiMalNode.getStateAt(i);
			if (stateName.equals("True")) {
				antiMalNode.addFinding(i);
			}
		}
		
		// propagate evidence
		System.out.println("\n -------- propagate the evidence....  -------- \n");
		alg.propagate();
		
		
		
		for (Node node : net.getNodes()) {
			System.out.println(node.getDescription());
			for (int i = 0; i < node.getStatesSize(); i++) {
				if (node instanceof TreeVariable) {
					System.out.println(node.getStateAt(i) + " : " 
							+ ((TreeVariable)node).getMarginalAt(i));
				}
			}
		}
		
		
		
		

		

		
	}
	
	


}
