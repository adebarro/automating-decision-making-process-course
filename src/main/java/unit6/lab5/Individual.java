package unit6.lab5;

import org.apache.commons.math3.distribution.BetaDistribution;

public class Individual {
	String name;
	int count;
	int alpha;
	int beta;
	double mean;
	double variance;
	double stdv;
	double perc_5;
	double perc_95;
	
	public Individual(String name, int alpha, int beta) {
		this.name = name;
		BetaDistribution bDist = new BetaDistribution(alpha , beta, 1e-9);
    	mean = bDist.getNumericalMean();
    	variance = bDist.getNumericalVariance();
    	stdv = Math.sqrt(variance);
    	perc_5 =bDist.inverseCumulativeProbability(0.05);
    	perc_95 = bDist.inverseCumulativeProbability(0.95);
	}

	public String getName() {
		return name;
	}

	public int getCount() {
		return count;
	}

	public int getAlpha() {
		return alpha;
	}

	public int getBeta() {
		return beta;
	}

	public double getMean() {
		return mean;
	}

	public double getVariance() {
		return variance;
	}

	public double getStdv() {
		return stdv;
	}

	public double gePerc5() {
		return perc_5;
	}

	public double getPerc95() {
		return perc_95;
	}
	

}
