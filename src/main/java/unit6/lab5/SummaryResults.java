package unit6.lab5;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

//https://github.com/dpaukov/combinatoricslib3

public class SummaryResults {
	
	public static final String FOLDER = "src/main/resources/";
	protected Hashtable<String, Integer> counting;
	
	
	protected long total = 0;

	public void parseCSV(String fileName) {
		try {
			// Create object of filereader
			// class with csv file as parameter.
			FileReader filereader = new FileReader(FOLDER+fileName);
			
			counting = new Hashtable<String, Integer>();
			
			// create csvParser object with
			// custom seperator semi-colon
			CSVParser parser = new CSVParserBuilder().withSeparator(',').build();

			// create csvReader object with
			// parameter filereader and parser
			CSVReader csvReader = new CSVReaderBuilder(filereader).withSkipLines(1).withCSVParser(parser).build();
			
			
			// we are going to read data line by line
			String[] nextRecord;
			String record="";
			while ((nextRecord = csvReader.readNext()) != null) {
				for (String cell : nextRecord) {
					record = record+cell;
					record=record+ "\t";
				}
				save(record);
				
				record="";
			}
			
			total = csvReader.getLinesRead();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public void save(String record) {
		String input[] = record.split("\t"); 
		
		String param="";
		for (String value : input) {
			param=param+value+";";
			int number=1;
			if (counting.containsKey(param)) {
				number = counting.get(param);
				number++;
			}
			
			counting.put(param, number);
		}
	}
	
	public void printGeneralStatistics () {
		Set<String> keys = counting.keySet();
		 
	    //Obtaining iterator over set entries
	    Iterator<String> itr = keys.iterator();
	 
	    //Displaying Key and value pairs
	    while (itr.hasNext()) { 
	       // Getting Key
	       String str = itr.next();
	       int count = getCount(str);
	       
	       String []prtArr = str.split("\t");
	       String prt = prtArr[0];
	       for (int i=1; i<prtArr.length;i++) {
	    	   prt =prt+ "-"+prtArr[i];
	       }
	       System.out.println(prt+":"+count);
	    } 
        
	}
	
	//array ={"a;b;c", "d;e;f", }
	public List<Individual> calcStatistics(int priorAlpha, int priorBeta, int dimension ) {
		List<Individual> indL = new ArrayList<Individual>();
		Set<String> keys = counting.keySet();
		int total_count = 0;

	 
		//Obtaining iterator over set entries
	    Iterator<String> itr = keys.iterator();
	    //Hashtable<Integer,String> statsDb = new Hashtable<>(); 
	    while (itr.hasNext()) {
	    	String value = itr.next();
	    	String[] nValue = value.split(";");
	    	if (nValue.length==dimension) {
	    		//System.out.println(value);
	    		int count = counting.get(value);
	    		System.out.println(value+":"+count);
	    		total_count = total_count + count;
	    	}
	    }
	    
	    itr = keys.iterator();
	    while (itr.hasNext()) {
	    	String name = itr.next();
	    	
	    	String[] nValue = name.split(";");
	    	if (nValue.length==dimension) {
		    	int count = counting.get(name);
		    	
		    	int alpha = priorAlpha+count;
		    	int beta = priorBeta+total_count-count;
		    	
		    	Individual ind = new Individual(name,alpha, beta);
		    	indL.add(ind);
	    	}
	    }
	    
	    return indL;

	}
	
	
	/*public double getStatistics (String  params) {
		int count = 0;
		if (counting.containsKey(params))
			count = counting.get(params);
		return (double)count/(double)total;
	}*/
	
	
	private int getCount (String params) {
		int count = 0;
		if (counting.containsKey(params))
			count = counting.get(params);
		return count;
	}
	
	public int getCount (String[] paramsArr) {
		String params="";
		for (String item : paramsArr) {
			params=params+item+";";
		}
		
		int count = 0;
		if (counting.containsKey(params))
			count = counting.get(params);
		return count;
	}
	
	public static void main (String args[]) {
		SummaryResults smResults = new SummaryResults();
		String fileName = "spy_data.csv";
		String params []= {"Supporter","Female"};
		smResults.parseCSV(fileName);
	//	smResults.printGeneralStatistics();
		int count = smResults.getCount(params);
		System.out.println(count);
		List<Individual> res = smResults.calcStatistics(1, 3,2);
		
		for (Individual ind : res) {
			System.out.println(ind.getName()+"- mean="+ind.getMean()+ ",95% perc="+ind.getPerc95());
		}
		
		
	}

	
	
}
