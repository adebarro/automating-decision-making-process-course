package unit7.lab6;

import net.sourceforge.jFuzzyLogic.FIS;

public class TestFoodService {

	public static final String FOLDER = "src/main/resources/";

	public static void main(String[] args) {
		// Load from 'FCL' file
        String foodService = FOLDER+"food_service.fcl";
        
        FIS fis = FIS.load(foodService,true);
        // Error while loading?
        if( fis == null ) { 
            System.err.println("Can't load file: '" 
                                   + foodService + "'");
            return;
        }
        
        // Show 
        fis.chart();

        // Set inputs
        fis.setVariable("service", 4);
        fis.setVariable("food", 2);

        // Evaluate
        fis.evaluate();

        // Show output variable's chart 
        fis.getVariable("tip").chartDefuzzifier(true);

        // Print ruleSet
        System.out.println(fis);
    }
        

}
