package unit7.lab6;

import java.util.Iterator;
import java.util.List;

import org.paukov.combinatorics3.Generator;


//src = https://github.com/dpaukov/combinatoricslib3
public class RulesGenerator {

	public static void generateCombinactorics(String[] input_variables, String[] input_values, String connector,
			String output_variable, int dimension) {
		
		Iterator<List<String>> it = Generator.combination(input_values).multi(dimension).stream().iterator();

		int count = 1;

		while (it.hasNext()) {
			List<String> combination = it.next();
			Iterator<List<String>> it2 = Generator.permutation(combination).simple().iterator();

			while (it2.hasNext()) {

				List<String> result = it2.next();
				String line = "RULE " + count + " : IF ";

				for (int i = 0; i < result.size(); i++) {
					String varI = input_variables[i];
					String value = result.get(i);

					line = line + varI + " IS " + value;

					if (i == result.size() - 1) {
						line = line + " ";
					}

					else {
						line = line + " " + connector + " ";
					}

				}

				line = line + " THEN " + output_variable + " IS ?;";
				count++;
				System.out.println((line));
			}

		}

	}

	public static void main(String args[]) {
		String[] input_variables = { "overal_likelihood", "impact" };
		String[] input_values = { "very_low", "low", "medium", "high", "very_high" };
		String output_variables = "risk";

		generateCombinactorics(input_variables, input_values, "AND", output_variables, 2);
	}

}
