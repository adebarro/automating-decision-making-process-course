package unit7.lab6;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.rule.Variable;

public class RiskCalculator2 {
	public static final String FOLDER = "src/main/resources/";

	public static void main(String[] args) {
		// Load from 'FCL' file
        
        FIS fisBox1 = FIS.load(FOLDER+"risk_assessment.fcl",true);
        
        if( fisBox1 == null ) { 
            System.err.println("Can't load file!");
            return;
        }
        
        // Show 
        fisBox1.chart();
      
        
        //CALCULATION BOX 1
        // Set inputs
        fisBox1.setVariable("box_risk1","intent", 8.0);
        fisBox1.setVariable("box_risk1","targeting", 9.0);
        fisBox1.setVariable("box_risk1","capabilities", 8.0);
        
        // Evaluate
        fisBox1.evaluate();

        // Show output variable's chart 
        Variable over_cap = fisBox1.getFunctionBlock("box_risk1").getVariable("overal_capabilities");
        double v_ocer_capb = over_cap.getValue();
        over_cap.chartDefuzzifier(true);

        
        /// CALCULATION BOX 2
        fisBox1.setVariable("box_risk2","overal_capabilities", v_ocer_capb);
        fisBox1.setVariable("box_risk2","vulnerabilities", 18.0);
        
        fisBox1.evaluate();
        
        Variable overal_likelihood = fisBox1.getFunctionBlock("box_risk2").getVariable("overal_likelihood");
        double v_likelihood = overal_likelihood.getValue();
        overal_likelihood.chartDefuzzifier(true);
        
        
        //CALCULATION BOX 3
        fisBox1.setVariable("box_risk3","overal_likelihood", v_likelihood);
        fisBox1.setVariable("box_risk3","impact", 18.0);
        
        fisBox1.evaluate();
        
        Variable risk = fisBox1.getFunctionBlock("box_risk3").getVariable("risk");
        risk.chartDefuzzifier(true);
	}

}
