package unit7.lab7;

import java.util.ArrayList;
import java.util.List;

import fr.ign.cogit.evidence.configuration.Configuration;
import fr.ign.cogit.evidence.configuration.ConfigurationSet;
import fr.ign.cogit.evidence.massvalues.MassPotential;
import fr.ign.cogit.evidence.variable.Variable;
import fr.ign.cogit.evidence.variable.VariableFactory;
import fr.ign.cogit.evidence.variable.VariableSet;

public class BaltimorePort {
	public static void main(String args[]) {

		// instantiate a variable factory
		VariableFactory<String> vf = new VariableFactory<String>();

		// create the variable vessel types
		Variable<String> vessel_types = vf.newVariable();
		vessel_types.add("freighter");
		vessel_types.add("fishing boat");
		vessel_types.add("other");

		// create the variable vessel types
		Variable<String> speed = vf.newVariable();
		speed.add("vessel is above docking speed");
		speed.add("vessel is below docking speed");

		// creation of port
		VariableSet<String> port_auth = new VariableSet<String>(vf);
		port_auth.add(vessel_types);
		port_auth.add(speed);

		/********	-	Lazaretto sensor 	************************/
		VariableSet<String> sens1LazVar = new VariableSet<String>(vf);
		sens1LazVar.add(vessel_types);

		List<String> evSens1Laz = new ArrayList<String>();
		evSens1Laz.add("other");
		
		ConfigurationSet<String> configSetSens1Laz = new ConfigurationSet<String>(sens1LazVar);
		Configuration<String> configSens1Laz_1 = new Configuration<String>(sens1LazVar, evSens1Laz);
		configSetSens1Laz.add(configSens1Laz_1);

		ConfigurationSet<String> configSetSens1Laz_2 = new ConfigurationSet<String>(sens1LazVar);
		configSetSens1Laz_2.addAllConfigurations();

		MassPotential<String> sens1Laz = new MassPotential<String>(sens1LazVar);
		sens1Laz.add(configSetSens1Laz, 0.9);
		sens1Laz.add(configSetSens1Laz_2, 1 - 0.9);
		sens1Laz.check();
		System.out.println("Sensor 1 Laz:\n" + sens1Laz);

		/********	-	-	Fort McHenry sensor  	************************/
		VariableSet<String> sens2FMVar = new VariableSet<String>(vf);
		sens2FMVar.add(vessel_types);

		List<String> evSens2FM = new ArrayList<String>();
		evSens2FM.add("freighter");
		
		ConfigurationSet<String> configSetSens2FM_1 = new ConfigurationSet<String>(sens2FMVar);
		Configuration<String> configSens2FM_1 = new Configuration<String>(sens2FMVar, evSens2FM);
		configSetSens2FM_1.add(configSens2FM_1);

		ConfigurationSet<String> configSetSens2FM_2 = new ConfigurationSet<String>(sens2FMVar);
		configSetSens2FM_2.addAllConfigurations();

		MassPotential<String> sens2FM = new MassPotential<String>(sens2FMVar);
		sens2FM.add(configSetSens2FM_1, 0.8);
		sens2FM.add(configSetSens2FM_2, 1 - 0.8);
		sens2FM.check();
		System.out.println("Sensor 2 FM:\n" + sens2FM);

		/********		COMBINATION -> Lazaretto sensor && Fort McHenry sensor  	************************/
		MassPotential<String> finalPotential = sens1Laz.combination(sens2FM);
		List<String> lfinal = new ArrayList<String>();
		//lfinal.add("other");
		//lfinal.add("freighter");
		
		Configuration<String> configFinal = new Configuration<String>(port_auth, lfinal);
		ConfigurationSet<String> ffinal = new ConfigurationSet<String>(port_auth);
		ffinal.add(configFinal);
		System.out.println("Sensor 1 + Sensor 2:\n" + finalPotential);

		/********		-	Lazaretto sensor (Doppler)   	************************/
		VariableSet<String> sens3DopLazVar = new VariableSet<String>(vf);
		sens3DopLazVar.add(speed);

		List<String> sens3DopLazEv = new ArrayList<String>();
		sens3DopLazEv.add("vessel is below docking speed");

		Configuration<String> configSens3DopLaz_1 = new Configuration<String>(sens3DopLazVar, sens3DopLazEv);
		ConfigurationSet<String> configSetSens3DopLaz_1 = new ConfigurationSet<String>(sens3DopLazVar);
		configSetSens3DopLaz_1.add(configSens3DopLaz_1);

		ConfigurationSet<String> configSetSens3DopLaz_2 = new ConfigurationSet<String>(sens3DopLazVar);
		configSetSens3DopLaz_2.addAllConfigurations();

		MassPotential<String> sens3DopLaz = new MassPotential<String>(sens3DopLazVar);
		sens3DopLaz.add(configSetSens3DopLaz_1, 0.75);
		sens3DopLaz.add(configSetSens3DopLaz_2, 1 - 0.75);
		sens3DopLaz.check();
		System.out.println("Sensor 3 Doppler Laz:\n" + sens3DopLaz);

		/********	COMBINATION -> Lazaretto sensor && Fort McHenry sensor && -	Lazaretto sensor (Doppler)	************************/
		MassPotential<String> finalPotential2 = finalPotential.combination(sens3DopLaz);
		List<String> lfinal2 = new ArrayList<String>();
		//lfinal2.add("other");
		//lfinal2.add("freighter");
		//lfinal2.add("vessel is below docking speed");

		Configuration<String> configFinal2 = new Configuration<String>(port_auth, lfinal2);
		ConfigurationSet<String> ffinal2 = new ConfigurationSet<String>(port_auth);
		ffinal2.add(configFinal2);
		System.out.println("Sensor 1 + Sensor 2 + Sensor 3:\n" + finalPotential2);

		// calculation
		double belief = finalPotential.bel(ffinal);
		System.out.println("Bel(Freighter)= " + belief);

		double plausibility = finalPotential.pls(ffinal);
		System.out.println("Pls(Freighter) = " + plausibility);

		double doubt = finalPotential.dou(ffinal);
		System.out.println("Dou(Freighter) = " + doubt);

		double communality = finalPotential.com(ffinal);
		System.out.println("Com(Freighter) = " + communality);

		double ignorance = finalPotential.ign(ffinal);
		System.out.println("Ign(Freighter) = " + ignorance);
	
	}
}
