package unit7.lab7;

import java.util.ArrayList;
import java.util.List;

import fr.ign.cogit.evidence.configuration.Configuration;
import fr.ign.cogit.evidence.configuration.ConfigurationSet;
import fr.ign.cogit.evidence.massvalues.MassPotential;
import fr.ign.cogit.evidence.variable.Variable;
import fr.ign.cogit.evidence.variable.VariableFactory;
import fr.ign.cogit.evidence.variable.VariableSet;

public class ClueGame {

	public static void main(String args[]) {

		// instantiate a variable factory
		VariableFactory<String> vf = new VariableFactory<String>();

		// create the variable murderer
		Variable<String> murderer = vf.newVariable();
		murderer.add("Colonel Mustard");
		murderer.add("Miss Scarlett");
		murderer.add("Mrs Peacock");

		// create the rooms
		Variable<String> room = vf.newVariable();
		room.add("Dining room");
		room.add("Kitchen");

		// create the weapons
		Variable<String> weapon = vf.newVariable();
		weapon.add("Dagger");
		weapon.add("Candlestick");

		// creation of game
		VariableSet<String> game = new VariableSet<String>(vf);
		game.add(murderer);
		game.add(room);
		game.add(weapon);

		// first player
		VariableSet<String> varSetP1 = new VariableSet<String>(vf);
		varSetP1.add(murderer);
		varSetP1.add(room);

		List<String> evP1 = new ArrayList<String>();
		evP1.add("Colonel Mustard");
		evP1.add("Kitchen");

		Configuration<String> configP1_1 = new Configuration<String>(varSetP1, evP1);
		ConfigurationSet<String> configSetP1_1 = new ConfigurationSet<String>(varSetP1);
		configSetP1_1.add(configP1_1);

		ConfigurationSet<String> configSetP1_2 = new ConfigurationSet<String>(varSetP1);
		configSetP1_2.addAllConfigurations();

		MassPotential<String> player1 = new MassPotential<String>(varSetP1);
		player1.add(configSetP1_1, 0.8);
		player1.add(configSetP1_2, 1-0.8);
		player1.check();
		System.out.println("Player 1:\n" + player1);

		// second player
		VariableSet<String> varSetP2 = new VariableSet<String>(vf);
		varSetP2.add(murderer);
		varSetP2.add(weapon);

		List<String> evP2 = new ArrayList<String>();
		evP2.add("Colonel Mustard");
		evP2.add("Candlestick");

		Configuration<String> configP2_1 = new Configuration<String>(varSetP2, evP2);
		ConfigurationSet<String> configSetP2_1 = new ConfigurationSet<String>(varSetP2);
		configSetP2_1.add(configP2_1);

		ConfigurationSet<String> configSetP2_2 = new ConfigurationSet<String>(varSetP2);
		configSetP2_2.addAllConfigurations();

		MassPotential<String> player2 = new MassPotential<String>(varSetP2);
		player2.add(configSetP2_1, 0.8);
		player2.add(configSetP2_2, 1-0.8);
		player2.check();
		System.out.println("Player 2:\n" + player2);

		// combination
		MassPotential<String> finalPotential = player1.combination(player2);
		List<String> lfinal = new ArrayList<String>();
		lfinal.add("Colonel Mustard");
		lfinal.add("Kitchen");
		lfinal.add("Candlestick");
		Configuration<String> configFinal = new Configuration<String>(game, lfinal);
		ConfigurationSet<String> ffinal = new ConfigurationSet<String>(game);
		ffinal.add(configFinal);
		System.out.println("Player 1 + Player 2:\n" + finalPotential);

		// calculation
	    double belief = finalPotential.bel(ffinal);
		System.out.println("Bel = " + belief);
		
		double plausibility = finalPotential.pls(ffinal);
		System.out.println("Pls = " + plausibility);
		
		double doubt = finalPotential.dou(ffinal);
		System.out.println("Dou = " + doubt);
		
		double communality = finalPotential.com(ffinal);
		System.out.println("Com = " + communality);
		
		double ignorance = finalPotential.ign(ffinal);
		System.out.println("Ign = " + ignorance);

	}

}
